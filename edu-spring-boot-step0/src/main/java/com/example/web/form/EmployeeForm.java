package com.example.web.form;

import com.example.persistence.entity.Employee;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;
import org.hibernate.validator.constraints.Length;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDate;

public class EmployeeForm {

    @NotBlank
    @Length(max = 10)
    private String name;

    @NotNull
    @DateTimeFormat(pattern = "uuuu-MM-dd")
    private LocalDate joinedDate;

    @NotBlank
    @Length(max = 20)
    private String departmentName;

    @NotBlank
    @Email
    @Length(max = 256)
    private String email;

    @NotNull
    @PastOrPresent
    @DateTimeFormat(pattern = "uuuu-MM-dd")
    private LocalDate birthDay;

    private MultipartFile image;

    public EmployeeForm(){
    }

    public EmployeeForm(Employee employee){
        name = employee.getName();
        joinedDate = employee.getJoinedDate();
        departmentName = employee.getDepartmentName();
        email = employee.getEmail();
        birthDay = employee.getBirthDay();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getJoinedDate() {
        return joinedDate;
    }

    public void setJoinedDate(LocalDate joinedDate) {
        this.joinedDate = joinedDate;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public LocalDate getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(LocalDate birthDay) {
        this.birthDay = birthDay;
    }

    public MultipartFile getImage() {
        return image;
    }

    public void setImage(MultipartFile image) {
        this.image = image;
    }

    public Employee convertToEntity() {
        return new Employee(null, name, joinedDate, departmentName, email, birthDay, null);
    }

    @Override
    public String toString() {
        return "EmployeeForm{" +
                "name='" + name + '\'' +
                ", joinedDate=" + joinedDate +
                ", departmentName='" + departmentName + '\'' +
                ", email='" + email + '\'' +
                ", birthDay=" + birthDay +
                ", image=" + image +
                '}';
    }
}
