package com.example.web.controller;

import com.example.persistence.entity.Employee;
import com.example.service.EmployeeService;
import com.example.web.form.EmployeeForm;
import com.example.web.session.EmployeeId;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

@Controller
@RequestMapping("/employee")
public class EmployeeController {
    private final EmployeeService employeeService;

    private final EmployeeId employeeId;

    public EmployeeController(EmployeeService employeeService, EmployeeId employeeId) {
        this.employeeService = employeeService;
        this.employeeId = employeeId;
    }

    /**
     * 全件検索を行い、一覧画面に遷移する。
     */
    @GetMapping("/index")
    public String index(Model model, @PageableDefault(size = 5) Pageable pageable) {
        Page<Employee> page = employeeService.findAll(pageable);
        model.addAttribute("page", page);
        model.addAttribute("employeeList", page.getContent());
        return "employee/index";
    }

    /**
     * 名前キーワード検索を行い、一覧画面に遷移する。
     */
    @GetMapping("/findById")
    public String findById(@RequestParam Integer id, Model model) {
        Employee employee = employeeService.findById(id);
        model.addAttribute("employee", employee);
        return "employee/findById";
    }

    /**
     * 新規追加 入力画面に遷移する。
     */
    @GetMapping("/insertMain")
    public String insertMain(Model model) {
        model.addAttribute("employeeForm", new EmployeeForm());
        return "employee/insertMain";
    }

    /**
     * 入力を受け取り、DBへの追加を実行する。
     * 追加処理完了後は、一覧画面にリダイレクトする。
     */
    @PostMapping("/insertComplete")
    public String insertComplete(@Validated EmployeeForm employeeForm, BindingResult result) {
        if (result.hasErrors()) {
            return "employee/insertMain";
        }

        Employee employee = employeeForm.convertToEntity();
        MultipartFile image = employeeForm.getImage();
        Resource imageResource = image.isEmpty() ? null : image.getResource();

        employeeService.insert(employee, imageResource);
        return "redirect:index";
    }

    /**
     * 社員情報　更新編集画面に遷移する。
     */
    @GetMapping("/updateMain")
    public String updateMain(@RequestParam Integer id, Model model) {
        Employee employee = employeeService.findById(id);
        EmployeeForm employeeForm = new EmployeeForm(employee);
        model.addAttribute("employeeForm", employeeForm);
        model.addAttribute("employeeId", id);
        // SessionScopeに社員idを保持
        employeeId.setId(id);
        return "employee/updateMain";
    }

    /**
     * 入力を受け取り、DBへの更新を実行する。
     * 追加処理完了後は、一覧画面にリダイレクトする。
     */
    @PostMapping("/updateComplete")
    public String updateComplete(@Validated EmployeeForm employeeForm, BindingResult result) {
        if (result.hasErrors()) {
            return "employee/updateMain";
        }

        Employee employee = employeeForm.convertToEntity();
        // SessionScopeから社員IDを取得
        employee.setId(employeeId.getId());
        MultipartFile image = employeeForm.getImage();
        Resource imageResource = image.isEmpty() ? null : image.getResource();

        employeeService.update(employee, imageResource);
        return "redirect:index";
    }

    /**
     * 社員情報　削除確認画面に遷移する。
     */
    @GetMapping("/deleteConfirm")
    public String deleteConfirm(@RequestParam Integer id, Model model) {
        Employee employee = employeeService.findById(id);
        System.out.println(employee);
        model.addAttribute("employee", employee);
        // SessionScopeに社員idを保持
        employeeId.setId(id);
        return "employee/deleteConfirm";
    }

    /**
     * 入力を受け取り、DBへの更新を実行する。
     * 追加処理完了後は、一覧画面にリダイレクトする。
     */
    @PostMapping("/deleteComplete")
    public String deleteComplete(Model model) {
        // SessionScopeから社員IDを取得
        Integer id = employeeId.getId();
        employeeService.delete(id);
        return "redirect:index";
    }
}
