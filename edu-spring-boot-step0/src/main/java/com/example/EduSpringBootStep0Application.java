package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EduSpringBootStep0Application {

    public static void main(String[] args) {
        SpringApplication.run(EduSpringBootStep0Application.class, args);
    }

}
