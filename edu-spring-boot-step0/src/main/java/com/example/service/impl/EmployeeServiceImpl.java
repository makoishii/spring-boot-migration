package com.example.service.impl;

import com.example.exception.EmployeeNotFoundException;
import com.example.exception.StorageException;
import com.example.persistence.entity.Employee;
import com.example.persistence.repository.EmployeeRepository;
import com.example.service.EmployeeService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.FileCopyUtils;
import org.springframework.util.FileSystemUtils;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.util.List;
import java.util.Optional;

@Service
public class EmployeeServiceImpl implements EmployeeService {
    private final String imagePathPrefix;

    private final String imageStorageLocation;

    private final EmployeeRepository employeeRepository;

    public EmployeeServiceImpl(EmployeeRepository employeeRepository,
                               @Value("${image.path.prefix}") String imagePathPrefix,
                               @Value("${image.storage.location}") String imageStorageLocation) {
        this.employeeRepository = employeeRepository;
        this.imagePathPrefix = imagePathPrefix;
        this.imageStorageLocation = imageStorageLocation;
    }

    @Override
    @Transactional(readOnly = true)
    public List<Employee> findAll() {
        List<Employee> employeeList = employeeRepository.findAll();
        return employeeList;
    }

    @Override
    @Transactional(readOnly = true)
    public List<Employee> findAll(Sort sort) {
        List<Employee> employeeList = employeeRepository.findAll(sort);
        return employeeList;
    }

    // 追加 一覧ページでページネーション対応
    @Override
    @Transactional(readOnly = true)
    public Page<Employee> findAll(Pageable pageable) {
        Page<Employee> employeeList = employeeRepository.findAll(pageable);
        return employeeList;
    }

    @Override
    @Transactional(readOnly = true)
    public Employee findById(Integer id) {
        Optional<Employee> employeeOpt = employeeRepository.findById(id);
        // 存在しない場合は、例外を返す
        return employeeOpt.orElseThrow(
                () -> {
                    throw new EmployeeNotFoundException("ID:"+ id.toString() + "の社員データは存在しません、直前に削除された可能性があります。");
                }
        );
    }

    @Override
    @Transactional
    public void insert(Employee employee, Resource imageResource) {
        Integer id = employee.getId();
        if(id != null){
            if(employeeRepository.existsById(id)){
                throw new IllegalArgumentException("[内部エラー]新規追加:既に登録されているid(" + id + ")で新規追加することはできません");
            }
        }
        Employee newEmployee = employeeRepository.save(employee);
        if (imageResource != null) {
            newEmployee.setImagePath(imagePathPrefix + "/" + newEmployee.getId() + ".png");
            try (InputStream inputStream = imageResource.getInputStream()) {
                FileCopyUtils.copy(inputStream.readAllBytes(),
                        Path.of(imageStorageLocation, newEmployee.getId() + ".png").toFile());
            } catch (IOException e) {
                throw new StorageException("登録処理に失敗しました。");
            }
        }
    }

    @Override
    @Transactional
    public void update(Employee employee, Resource imageResource) {
        Integer id = employee.getId();
        if (id == null) {
            throw new IllegalArgumentException("[内部エラー]更新:id nullで更新することはできません");
        }  else if (id < 1) {
            throw new IllegalArgumentException("[内部エラー]更新:idが不正な値です (id=" + id + ")");
        }
        Optional<Employee> targetOp = employeeRepository.findById(id);
        Employee target = targetOp.orElseThrow(
                // 取り出せなかったら、例外を返す。
                () -> {
                    throw new EmployeeNotFoundException("ID:"+ id.toString() + "の社員データは存在しないので更新できませんでした、直前に削除された可能性があります。");
                }
        );

        // 取得したtargetのsetterを使って、targetの内容を更新すると、DBに更新内容が反映されます。
        //   ※ employeeRepository.save(employee); を行う必要はありません。
        // (EntityManagerで管理しているオブジェクトのセッターを使って値を更新すれば自動的に永続化される)
        target.setName(employee.getName());
        target.setDepartmentName(employee.getDepartmentName());
        target.setJoinedDate(employee.getJoinedDate());
        target.setEmail(employee.getEmail());
        // 参考 以下のようにBeanUtils.copyPropertiesメソッドを使って記述することもできます。
        // 引数で渡されたemployeeのidを除いたフィールドをtargetにコピーする。
        //BeanUtils.copyProperties(employee, target, "id");
        if (imageResource != null) {
            target.setImagePath(imagePathPrefix + "/" + target.getId() + ".png");
            try (InputStream inputStream = imageResource.getInputStream()) {
                FileCopyUtils.copy(inputStream.readAllBytes(),
                        Path.of(imageStorageLocation, employee.getId() + ".png").toFile());
            } catch (IOException e) {
                throw new StorageException("更新処理に失敗しました。");
            }
        }

    }

    @Override
    @Transactional
    public void delete(Integer id) {
        if (id == null) {
            throw new IllegalArgumentException("[内部エラー]削除:id nullで削除することはできません");
        } else if (id < 1) {
            throw new IllegalArgumentException("[内部エラー]削除:idが不正な値です (id=" + id + ")");
        } else if(employeeRepository.existsById(id) == false){
            throw new EmployeeNotFoundException("ID:"+ id.toString() + "の社員データは存在しないので削除できませんでした、直前に削除された可能性があります。");
            // もしくは、システムの仕様によっては、エラーを無視しても良い。
        }
        employeeRepository.deleteById(id);
        try {
            boolean result = FileSystemUtils.deleteRecursively(Path.of(imageStorageLocation, id + ".png"));
            System.out.println(result ? "ファイルを削除しました" : "ファイルは存在しませんでした");
        } catch (IOException e) {
            throw new StorageException("削除処理に失敗しました。");
        }
    }
}
