/* Drop Tables */
DROP TABLE IF EXISTS employee_projects; /* for spring-data-jpa-moreover */
DROP TABLE IF EXISTS project;           /* for spring-data-jpa-moreover */
DROP TABLE IF EXISTS contact_address;   /* for spring-data-jdbc-moreover */
DROP TABLE IF EXISTS employee;
DROP TABLE IF EXISTS department;        /* for spring-data-jpa-moreover */

/* Create Tables */
CREATE TABLE employee
(
    id INTEGER PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(10),
    joined_date DATE,
    department_name VARCHAR(20),
    email VARCHAR(256),
    birth_day DATE,
    image_path VARCHAR(128)
);
