# spring-boot-migration

JSUG 勉強会 JSUG勉強会2024その1 Spring Boot 3 へのマイグレーション パートのサンプルコードです。

## ライセンス

LGPL

個人学習目的のサンプルコードです。自己責任のもと、ご利用ください。

## 必要な環境

JDK 17

    JDK 17以上(JDK 21など)がインストールされていれば、JDK 17の互換モードで動作するので問題ありません。

MySQL 8 (https://dev.mysql.com/downloads/mysql/)

事前にご準備ください。

## DB環境作成

本プロジェクト直下にある `create-schema-spring.sql` を実行すると、アプリケーションから DBアクセスが可能です。

```
DROP DATABASE IF EXISTS spring;
CREATE DATABASE spring CHARACTER SET utf8mb4;
DROP USER IF EXISTS 'user'@'localhost';
CREATE USER 'user'@'localhost' IDENTIFIED BY 'P@ssw0rd';
GRANT ALL PRIVILEGES ON spring.* to 'user'@'localhost';
USE spring;
SHOW DATABASES;
```

user というDBユーザー、spring という名前のdetabase が、MySQL上に既にある場合、削除して再作成します。

ご注意ください。

## 含まれているサブプロジェクト


### edu-spring-boot-step0

Spring Boot 2.7までバージョンアップする用途のサンプルプロジェクトです。

### edu-spring-boot-step0-sample

Spring Boot 2.7までのバージョンアップ済みのサンプルプロジェクトです。


### edu-spring-boot-migration

Spring Boot 2.7から 3.0に手動でバージョンアップする用途のサンプルアプリケーションです。

### edu-spring-boot-migration-sample

Spring Boot 2.7から 3.0に手動でバージョンアップ済みのサンプルアプリケーションです。


### edu-spring-boot-migrator

Spring Boot Migratorを使って自動でバージョンアップを試す用途のサンプルアプリケーションです。
