package com.example.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class GlobalExceptionHandler {
    private static final Logger log = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public String handleNoSuchEntityException(EmployeeNotFoundException exception, Model model){
        log.error(exception.getMessage(), exception);
        model.addAttribute("message", exception.getMessage());
        return "exception/businessException";
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public String handleIllegalArgumentException(IllegalArgumentException exception, Model model) {
        log.error(exception.getMessage(), exception);
        String message = "・・・"; // エラーページに表示させるメッセージ
        model.addAttribute("message", message);
        return "exception/systemException";
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public String handleException(Exception exception, Model model) {
        log.error("[予期しないエラー発生]:" + exception.getMessage(), exception);
        String message = "・・・"; // エラーページに表示させるメッセージ
        model.addAttribute("message", message);
        return "exception/systemException";
    }

}
