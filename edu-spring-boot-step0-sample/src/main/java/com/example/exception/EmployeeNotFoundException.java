package com.example.exception;

public class EmployeeNotFoundException extends RuntimeException{
    public EmployeeNotFoundException(String message){
        super(message);
    }

    public EmployeeNotFoundException(String message, Throwable throwable){
        super(message, throwable);
    }
}
