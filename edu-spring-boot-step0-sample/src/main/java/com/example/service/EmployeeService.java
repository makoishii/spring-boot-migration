package com.example.service;

import com.example.persistence.entity.Employee;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.List;

public interface EmployeeService {
    List<Employee> findAll();
    List<Employee> findAll(Sort sort);

    // 追加 一覧ページでページネーション対応
    Page<Employee> findAll(Pageable pageable);

    /**
     * ID検索(存在しない場合は、例外が返ります)
     * <p>存在しない場合に、例外ハンドラーなどで共通エラー処理を行う場合に利用できます。
     *
     * @param id
     * @return Employee
     * @throws  {@code NoSuchEntityException} 該当するEmployeeが存在しない場合
     */
    Employee findById(Integer id);
    void insert(Employee employee, Resource imageResource);
    void update(Employee employee, Resource imageResource);
    void delete(Integer id);
}
