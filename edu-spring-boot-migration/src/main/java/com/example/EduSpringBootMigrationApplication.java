package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EduSpringBootMigrationApplication {

    public static void main(String[] args) {
        SpringApplication.run(EduSpringBootMigrationApplication.class, args);
    }

}
